public class Logos
 {
	public static final int LEVEL_ASSERT = 1;
	public static final int LEVEL_VERBOSE = 2;
	public static final int LEVEL_INFO = 3;
	public static final int LEVEL_WARN = 4;
	public static final int LEVEL_ERROR = 5;
	public static final int LEVEL_CRITICAL = 6;
	
	private static Output output = new Output();
	
	public static void setOutput(final Output output) {
		Logos.output = output;
	}
	
	public static void log(final int LOG_LEVEL, final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
		if(output.Filter(LOG_LEVEL, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL))
		output.Callback(LOG_LEVEL, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL);
	}
	
	public static void log(final int LOG_LEVEL, final String LOG_TAG, final String LOG_DETAIL) {
		log(LOG_LEVEL, null, LOG_TAG, LOG_DETAIL);
	}
	
	public static void log(final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_ASSERT, null, LOG_TAG, LOG_DETAIL);
	}
	
	public static void a(final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_ASSERT, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL);
	}
	
	public static void a(final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_ASSERT, null, LOG_TAG, LOG_DETAIL);
	}
	
	public static void v(final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_VERBOSE, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL);
	}
	
	public static void v(final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_VERBOSE, null, LOG_TAG, LOG_DETAIL);
	}
	
	public static void i(final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_INFO, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL);
	}
	
	public static void i(final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_INFO, null, LOG_TAG, LOG_DETAIL);
	}
	
	public static void w(final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_WARN, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL);
	}
	
	public static void w(final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_WARN, null, LOG_TAG, LOG_DETAIL);
	}
	
	public static void e(final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_ERROR, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL);
	}
	
	public static void e(final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_ERROR, null, LOG_TAG, LOG_DETAIL);
	}
	
	public static void c(final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_CRITICAL, LOG_NAMESPACE, LOG_TAG, LOG_DETAIL);
	}
	
	public static void c(final String LOG_TAG, final String LOG_DETAIL) {
		log(LEVEL_CRITICAL, null, LOG_TAG, LOG_DETAIL);
	}
	
	
	public static class Output {
		public boolean Filter(final int LOG_LEVEL, final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
			//true log will be output
			return true;
		}
		
		
		public void Callback(final int LOG_LEVEL, final String LOG_NAMESPACE, final String LOG_TAG, final String LOG_DETAIL) {
			//default output callback
		}
	}
}
